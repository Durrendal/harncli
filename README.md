# HarnCLI

A dungeon master tool to handle all manners of game mechanisms for the Harnmaster 3rd Edition setting.

## TODO:

#### Functionality Goals:

- [] Database Structure
- [] Character Generation
- [x] Basic Dice Roller
- [] Magic Mechanics
- [] Travel Mechanics
- [] Combat Mechanics
- [] Religion Mechanics

##### Post Compilation Goals:
- [] Alpine Docker Build
- [] APKBUILD

## In Progress:

Currently building database structures, and populating them with information necessary to generate characters.

## Brief:

HarnCLI is a work in progress.

If anyone has any suggestions, please feel free to email me at wpsinatra@gmail.com.