(in-package harn_clos)

(defparameter *Sir_William* '(name "William WordsWorth" ;;choice
			      species "Human" ;;choice
			      sex "Male" ;;choice
			      birthdate "25th Agrazhar" ;;1d12 1d30
			      sunsign '*Nadai* ;;from birthdate
			      culture "Tharda" ;;choice
			      social_class "Unguilded" ;;1d100 + culture
			      sibling_rank "6th Child" ;;1d100
			      parent "Offspring, Both Parents" ;;1d100
			      estrangement "Favorite" ;;1d100
			      clanhead "Aunt/Uncle" ;;1d100
			      height 69 ;;4d6
			      frame 4 ;;3d6
			      weight 126 ;;(- 157 (/ (* 157 20) 100.00))
			      size 5 ;;from weight
			      CML 7 ;;3d6
			      complexion "fair" ;;1d100
			      hair_color "blond" ;;1d100
			      eye_color "blue" ;;1d100 +25 if fair human
			      STR 9 ;;3d6
			      STA 8 ;;3d6
			      DEX 11 ;;3d6
			      AGL 6 ;;3d6
			      EYE 7 ;;3d6
			      HRG 5 ;;3d6
			      SML 4 ;;3d6
			      VOI 8 ;;3d6
			      medical NIL ;;1d100
			      INT 8 ;;3d6
			      AUR 9 ;;3d6
			      WIL 6 ;;3d6
			      morality "9 - Corruptible" ;;3d6/choice
			      psyche "Severe" ;;1d100
			      sexuality "Bi-Sexual" ;;1d100/choice
			      mental_disorder "Multiple Personalities" ;;1d100
			      deity "Save-K'nor" ;;choice/morality based
			      piety 17 ;;5d6
			      parent_occupation "11 - Servant" ;;1d100 - deps social_class
			      occupation "Alchemist" ;;choice - deps parent_occupation
			      occupation_income "288d/yr 24d/m" ;;defparam of occupation
			      ))

