(uiop:define-package #:harn_clos
    (:use :cl :dbi :sxql :uiop)
  (:export :set-the-stage))

(in-package harn_clos)

;;===== Variables =====
(defvar databaseloc "/home/wsinatra/harn.sqlite3")
(defvar skill_def_tbl_id 0)
(defvar character_tbl_id 0)
(defvar char_phys_eml_id 0)
(defvar char_comm_eml_id 0)
(defvar char_rit_eml_id 0)
(defvar char_combat_eml_id 0)
(defvar char_craftlore_eml_id 0)
(defvar char_lang_eml_id 0)
(defvar char_pvar_eml_id 0)

;;===== Db Schema =====
;; Savorya AUR AUR INT 2
(defvar *skill_def_tbl* "create table skill_def_tbl (
id bigserial not null primary key, 
name text not null,
attr1 text not null, 
attr2 text not null, 
attr3 text not null, 
skill_base integer not null
)")

;; Character Biographical Information
(defvar *character_tbl* "create table character_tbl (
id bigserial not null primary key,
name text not null,
species text not null,
sex text not null,
birthdate text not null,
sunsign text not null,
birthplace text not null,
culture text not null,
social_class text not null,
sibling_rank text not null,
parent text not null,
estrangement text not null,
clanhead text not null,
height integer not null,
frame integer not null,
weight integer not null,
size integer not null,
comeliness integer not null,
complexion integer not null,
hair_color integer not null,
eye_color integer not null,
strength integer not null,
stamina integer not null,
dexterity integer not null,
agility integer not null,
eyesight integer not null,
hearing integer not null,
smell integer not null,
voice integer not null,
medical text,
intelligence integer not null,
aura integer not null,
will integer not null,
morality text not null,
psyche text,
sexuality text not null,
mental_disorder text,
deity text not null,
piety integer not null,
parent_occupation text,
occupation text not null,
occupation_income text not null
)")

;; Character Effective Mastery Levels
(defvar *char_physical_emls* "create table char_phys_emls (
id bigserial not null primary key,
character integer not null,
acrobatics integer null,
climbing integer not null,
condition integer not null,
dancing integer null,
jumping integer not null,
legerdemain integer null,
skiing integer null,
stealth integer not null,
swimming integer null,
throwing integer not null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_communication_emls* "create table char_comm_emls (
id bigserial not null primary key,
character integer not null,
acting integer null,
awareness integer not null,
intrigue integer not null,
lovecraft integer null,
mental_conflict integer null,
musician integer null,
oratory integer not null,
rhetoric integer not null,
singing integer not null,
language integer null,
script integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_ritual_emls* "create table char_rit_emls (
id bigserial not null primary key,
character integer not null,
agrik integer null,
halea integer null,
ilvir integer null,
larani integer null,
morgath integer null,
naveh integer null,
peoni integer null,
sarajin integer null,
saveknor integer null,
sien integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_combat_emls* "create table char_combat_emls (
id bigserial not null primary key,
character integer not null,
initiative integer not null,
unarmed integer not null,
riding integer null,
axe integer null,
blowgun integer null,
bow integer null,
dagger integer null,
flail integer null,
net integer null,
polearm integer null,
shield integer null,
sling integer null,
spear integer null,
sword integer null,
whip integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_craftlore_emls* "create table char_craftlore_emls (
id bigserial not null primary key,
character integer not null,
agriculture integer null,
alchemy integer null,
animalcraft integer null,
astrology integer null,
brewing integer null,
ceramics integer null,
cookering integer null,
drawing integer null,
embalming integer null,
engineering integer null,
fishing integer null,
fletching integer null,
folklore integer null,
foraging integer null,
glasswork integer null,
heraldry integer null,
herblore integer null,
hidework integer null,
jewelcraft integer null,
law integer null,
lockcraft integer null,
masonry integer null,
mathematics integer null,
metalcraft integer null,
milling integer null,
mining integer null,
perfumery integer null,
physician integer null,
piloting integer null,
runecraft integer null,
seamanship integer null,
shipwright integer null,
survival integer null,
tarotry integer null,
textilecraft integer null,
timbercraft integer null,
tracking integer null,
weaponcraft integer null,
weatherlore integer null,
woodcraft integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_lang_emls* "create table char_lang_emls (
id bigserial not null primary key,
character integer not null,
altish integer null,
emela integer null,
harnic integer null,
jarind integer null,
jarinese integer null,
old_altish integer null,
old_harnic integer null,
old_jarinese integer null,
orbaalese integer null,
yarili integer null,
harbaalese integer null,
ivinian integer null,
old_trierzi integer null,
palthanian integer null,
phari integer null,
quar integer null,
quarph integer null,
shorka integer null,
trierzi integer null,
azeri integer null,
azeryani integer null,
byrian integer null,
elbythian integer null,
high_azeryani integer null,
kerjian integer null,
karuia integer null,
lowazeryani integer null,
urmech integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *char_shekpvar_emls* "create table char_pvar_emls (
id bigserial not null primary key,
character integer not null,
lyahi integer null,
peleahn integer null,
jmorvi integer null,
fyvria integer null,
odvishe integer null,
savorya integer null,
neutral integer null,
foreign key (character) references character_tbl(id)
)")

(defvar *occupation_tbl* "create table occupation_tbl (
id bigserial not null primary key,
occupation text not null,
years integer not null,
skills text not null,
monthly text not null,
yearly text not null,
guilded text not null
)")

;;===== Utility =====
(defun sql-with (sql-query)
  (dbi:with-connection (conn :sqlite3 :database-name databaseloc)
    (let*
	((query (dbi:prepare conn sql-query))
	 (result (dbi:execute query)))
      (loop for row = (dbi:fetch result)
	 while row
	 collect row))))

(defun create-schema ()
  (sql-with *skill_def_tbl*)
  (sql-with *character_tbl*)
  (sql-with *char_physical_emls*)
  (sql-with *char_communication_emls*)
  (sql-with *char_ritual_emls*)
  (sql-with *char_combat_emls*)
  (sql-with *char_craftlore_emls*)
  (sql-with *char_lang_emls*)
  (sql-with *char_shekpvar_emls*)
  (sql-with *occupation_tbl*)
  (format t "Database Schema Populated~%"))

(defun count-of (table)
  (sql-with (concatenate 'string "select count(id) from " table ";")))

(defun add-skill (name attr1 attr2 attr3 sb)
  (let
      ((sql-data (count-of "skill_def_tbl")))
    (let
	((tbl_id (write-to-string (+ (nth 0 (cdr (pop sql-data))) 1))))
      (sql-with (concatenate 'string "insert into skill_def_tbl (id, name, attr1, attr2, attr3, skill_base) values (" "'" tbl_id "'" ", " "'" name "'" ", " "'" attr1 "'" ", " "'" attr2 "'" ", " "'" attr3 "'" ", " sb ");")))))

(defun populate-skill-defs ()
  ;; Physical Skills
  (format t "Populating Physical Skills~%")
  (add-skill "Acrobatics" "STR" "AGL" "AGL" "2")
  (add-skill "Climbing" "STR" "DEX" "AGL" "4")
  (add-skill "Condition" "STR" "STA" "WIL" "5")
  (add-skill "Dancing" "DEX" "AGL" "AGL" "2")
  (add-skill "Jumping" "STR" "AGL" "AGL" "4")
  (add-skill "Legerdemain" "DEX" "DEX" "WIL" "1")
  (add-skill "Skiing" "STR" "DEX" "AGL" "1")
  (add-skill "Stealth" "AGL" "HRG" "AGL" "3")
  (add-skill "Swimming" "STA" "DEX" "AGL" "1")
  (add-skill "Throwing" "STR" "DEX" "EYE" "4")
  ;; Communication Skills
  (format t "Populating Communication Skills~%")
  (add-skill "Acting" "AGL" "VOI" "INT" "2")
  (add-skill "Awareness" "EYE" "HRG" "SML" "4")
  (add-skill "Intrigue" "INT" "AUR" "WIL" "3")
  (add-skill "Lovecraft" "CML" "AGL" "VOI" "3")
  (add-skill "Mental Conflict" "AUR" "WIL" "WIL" "3")
  (add-skill "Musician" "DEX" "HRG" "HRG" "1")
  (add-skill "Oratory" "CML" "VOI" "INT" "2")
  (add-skill "Rhetoric" "VOI" "INT" "WIL" "3")
  (add-skill "Singing" "HRG" "VOI" "VOI" "3")
  (add-skill "Language" "VOI" "INT" "WIL" "1")
  (add-skill "Script" "DEX" "EYE" "INT" "1")
  ;; Ritual Skills
  (format t "Populating Ritual Skills~%")
  (add-skill "Agrik" "VOI" "INT" "STR" "1")
  (add-skill "Halea" "VOI" "INT" "CML" "1")
  (add-skill "Ilvir" "VOI" "INT" "AUR" "1")
  (add-skill "Larani" "VOI" "INT" "WIL" "1")
  (add-skill "Morgath" "VOI" "INT" "AUR" "1")
  (add-skill "Naveh" "VOI" "INT" "WIL" "1")
  (add-skill "Peoni" "VOI" "INT" "DEX" "1")
  (add-skill "Sarajin" "VOI" "INT" "STR" "1")
  (add-skill "Save Knor" "VOI" "INT" "INT" "1")
  (add-skill "Siem" "VOI" "INT" "AUR" "1")
  ;; Combat Skills
  (format t "Populating Combat Skills~%")
  (add-skill "Initiative" "AGL" "WIL" "WIL" "4")
  (add-skill "Unarmed" "STR" "DEX" "SGL" "4")
  (add-skill "Riding" "DEX" "AGL" "WIL" "1")
  (add-skill "Axe" "STR" "STR" "DEX" "3")
  (add-skill "Blowgun" "STA" "DEX" "EYE" "4")
  (add-skill "Bow" "STR" "DEX" "EYE" "2")
  (add-skill "Club" "STR" "STR" "DEX" "4")
  (add-skill "Dagger" "DEX" "DEX" "EYE" "3")
  (add-skill "Flail" "DEX" "DEX" "DEX" "1")
  (add-skill "Net" "DEX" "DEX" "EYE" "1")
  (add-skill "Polearm" "STR" "STR" "DEX" "2")
  (add-skill "Shield" "STR" "DEX" "DEX" "3")
  (add-skill "Sling" "DEX" "DEX" "EYE" "1")
  (add-skill "Spear" "STR" "STR" "DEX" "3")
  (add-skill "Sword" "STR" "DEX" "DEX" "3")
  (add-skill "Whip" "DEX" "DEX" "EYE" "1")
  ;; Craft & Lore Skills
  (format t "Populating Craft & Lore Skills~%")
  (add-skill "Agriculture" "STR" "STA" "WIL" "2")
  (add-skill "Alchemy" "SML" "INT" "AUR" "1")
  (add-skill "Animalcraft" "AGL" "VOI" "WIL" "1")
  (add-skill "Astrology" "EYE" "INT" "AUR" "1")
  (add-skill "Brewing" "DEX" "SML" "SML" "2")
  (add-skill "Ceramics" "DEX" "DEX" "EYE" "2")
  (add-skill "Cookery" "DEX" "SML" "SML" "3")
  (add-skill "Drawing" "DEX" "EYE" "EYE" "2")
  (add-skill "Embalming" "DEX" "EYE" "SML" "1")
  (add-skill "Engineering" "DEX" "INT" "INT" "1")
  (add-skill "Fishing" "DEX" "EYE" "WIL" "3")
  (add-skill "Fletching" "DEX" "DEX" "EYE" "1")
  (add-skill "Folklore" "VOI" "INT" "INT" "1")
  (add-skill "Foraging" "DEX" "SML" "INT" "3")
  (add-skill "Glasswork" "DEX" "EYE" "WIL" "1")
  (add-skill "Heraldry" "DEX" "EYE" "WIL" "1")
  (add-skill "Herblore" "EYE" "SML" "INT" "1")
  (add-skill "Hidework" "DEX" "SML" "INT" "2")
  (add-skill "Jewelcraft" "DEX" "EYE" "WIL" "1")
  (add-skill "Law" "VOI" "INT" "WIL" "1")
  (add-skill "Lockcraft" "DEX" "EYE" "WIL" "1")
  (add-skill "Masonry" "STR" "DEX" "INT" "1")
  (add-skill "Mathematics" "INT" "INT" "WIL" "1")
  (add-skill "Metalcraft" "STR" "DEX" "WIL" "1")
  (add-skill "Milling" "STR" "DEX" "SML" "2")
  (add-skill "Mining" "STR" "EYE" "INT" "1")
  (add-skill "Perfumery" "SML" "SML" "INT" "1")
  (add-skill "Physician" "DEX" "EYE" "INT" "1")
  (add-skill "Piloting" "DEX" "EYE" "INT" "1")
  (add-skill "Runecraft" "INT" "AUR" "AUR" "1")
  (add-skill "Seamanship" "STR" "DEX" "AGL" "1")
  (add-skill "Shipwright" "STR" "DEX" "INT" "1")
  (add-skill "Survival" "STR" "DEX" "INT" "3")
  (add-skill "Tarotry" "INT" "AUR" "WIL" "1")
  (add-skill "Textilecraft" "DEX" "DEX" "EYE" "2")
  (add-skill "Timbercraft" "STR" "DEX" "AGL" "2")
  (add-skill "Tracking" "EYE" "DEX" "WIL" "2")
  (add-skill "Weaponcraft" "STR" "DEX" "WIL" "1")
  (add-skill "Weatherlore" "INT" "EYE" "SML" "3")
  (add-skill "Woodcraft" "DEX" "DEX" "WIL" "2")
  ;; Shek'Pvar Convocational Skills
  (format t "Populating Shek'Pvar Skills~%")
  (add-skill "Lyahvi" "AUR" "AUR" "EYE" "2")
  (add-skill "Peleahn" "AUR" "AUR" "AGL" "2")
  (add-skill "Jmorvi" "AUR" "AUR" "STR" "2")
  (add-skill "Fyvria" "AUR" "AUR" "SML" "2")
  (add-skill "Odvishe" "AUR" "AUR" "DEX" "2")
  (add-skill "Savorya" "AUR" "AUR" "INT" "2")
  (add-skill "Neutral" "AUR" "AUR" "WIL" "2")
  (format t "Skills Populated to Database~%"))

(defun add-occ (occupation years skills monthly yearly guilded)
  (let
      ((sql-data (count-of "occupation_tbl")))
    (let
	((tbl_id (write-to-string (+ (nth 0 (cdr (pop sql-data))) 1))))
      (sql-with (concatenate 'string "insert into occupation_tbl (id, occupation, years, skills, monthly, yearly, guilded) values (" tbl_id ", " "'" occupation "'" ", " years ", " "'" skills "'" ", " "'" monthly "'" ", " "'" yearly "'" ", " "'" guilded "'" ");")))))

(defun populate-occupation-defs ()
  (add-occ "Animal Trainer" "3" "(Animalcraft 4 Hidework 2 Riding 3)" "72" "864" "no")
  (add-occ "Beggar" "1" "(Rhetoric 5 Intrigue 4 Dagger 4)" "variable" "variable" "no")
  (add-occ "Cartographer" "5" "(Drawing 4 Mathematics 2 Script)" "84" "1008" "no")
  (add-occ "Artist" "5" "(Drawing 4 Mathematics 2 Script)" "84" "1008" "no")
  ;; Clerics are per deit
  (add-occ "Shaman" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Agrik" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Azeryani aOML Zerin aOML Initiative 6 Unarmed 4 Sickle 4 Mace 4 Shield 4 Heraldry 3 Surikal 3)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Halea" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Lovecraft 4 Dancing 3 Mathematics 2 Musician 2 Perfumery 2 Karuia aOML Zerin aOML Lang2 aOML Script2 aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Ilvir" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Ivashucraft 4 Drawing 3 Physician 3 Tarotry 2 Herblore 2 Ivashi 3 Old_Jarinese aOML Khruni aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Larani" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Initiative 6 Unarmed 4 Dagger 4 Sword 4 Shield 4 Heraldry 3 Emela aOML Khruni aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Morgath" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Dagger 4 Embalming 3 Hideworking_HumanSkin 2 Woodworking 2 Tarotry 2 Ormauk 3 Azeri aOML Nuvesarl aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Naveh" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Initiative 6 Unarmed 5 Stealth 5 Dagger 4 Acrobatics 3 Legerdemain 2 Lockcraft 2 Besha aOML Neramic aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Peoni" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Agriculture 4 Unarmed 3 Herblore 3 Physician 3 Weatherlore 3 Textilecraft 2 Emela aOML Khruni aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Sarajin" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Initiative 6 Unarmed 4 Axe 4 Dagger 4 Shield 4 Runecraft 3 Seamanship 2 Ivinian aOML Runic aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Save-Knor" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Drawing 3 Mathematics 3 Law 3 Heraldry 2 Lang2 aOML Lang3 aOML Script2 aOML Azeri aOML Tianta aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cleric - Siem" "3" "(Ritual 4 Rhetoric 4 Intrigue 4 Mental_Conflict 4 Oratory 3 Folklore 3 Embalming 2 Physician 2 Law 2 Drawing 2 Heraldry 2 Language aOML Script aOML Astrology 4 Runecraft 3 Foraging 3 Survival 3 Weatherlore 3 Tarotry 2 Musician 2 Herblore 2 Sindarin aOML Selenian aOML)" "NIL" "NIL" "NIL")
  (add-occ "Cook" "3" "(Intrigue 4 Rhetoric 4 Cookery 4 Textilecraft 3 Herblore 2)" "30" "360" "no")
  (add-occ "Servant" "3" "(Intrigue 4 Rhetoric 4 Cookery 4 Textilecraft 3 Herblore 2)" "24" "288" "no")
  (add-occ "Farmer" "4" "(Agriculture 4 Weatherlore 4 Animalcraft 3)" "24" "288" "no")
  (add-occ "Fisherman" "5" "(Fishing 5 Seamanship 4 Weatherlore 4 Pilotting 2 Shipwright 2)" "48" "576" "no")
  ;; Gladiator opens with 3 weapon OMLs
  (add-occ "Herdsman" "2" "(Animalcraft 4 Tracking 3 Survival 3 Weatherlore 4)" "24" "288" "no")
  ;; Hunter opens with 3 weapon OMLs
  ;; Trapper opens with 3 weapon OMLs
  (add-occ "Laborer" "1" "(Intrigue 4)" "42" "504" "no")
  (add-occ "Longshoreman" "1" "(Intrigue 4 Seamanship 2)" "42" "504" "no")
  (add-occ "Prostitute" "1" "(Lovecraft 4 Intrigue 4 Dagger 4)" "variable" "variable" "no")
  (add-occ "Pimp" "1" "(Lovecraft 4 Intrigue 4 Dagger 4)" "variable" "variable" "no")
  (add-occ "Ratter" "3" "(Ratcraft 5 Dogcraft 4 Net 3 Club 5 Herblore 2)" "60" "720" "no")
  ;; Sage opens with 2nd lang + 2x scripts
  ;; Tutor opens with 2nd lang + 2x scripts
  ;; Scribe opens with 2nd lang + 3x scripts
  (add-occ "Teamster" "2" "(Riding 4 Animalcraft 3 Whip 3 Woodcraft 2)" "72" "864" "no")
  (add-occ "Thatcher" "4" "(Climbing 5 Woodcraft 3 Engineering 2)" "54" "648" "no")
  (add-occ "Toymaker" "5" "(Woodcraft 4 Lockcraft 3 Drawing 3 Hidework 2)" "48" "576" "no")
  (add-occ "Alchemist" "7" "(Alchemy 4 Mathematics 3 Herblore 3 Mining 2 Physician 2 Script aOML)" "NIL" "NIL" "NIL")
  (add-occ "Apothecary" "6" "(Herblore 4 Alchmey 2 Mathematics 2 Physician 2 Script aOML)" "60" "720" "yes")
  (add-occ "Astrologer" "4" "(Astrology 4 Mathematics 3 Drawing 3 Tarotry 2 Script aOML)" "NIL" "NIL" "NIL")
  ;; Chandler opens with 3x crafts
  (add-occ "Charcoaler" "3" "(Timbercraft 3 Survival 3 Woodcraft 2 Metalcraft 2)" "54" "648" "yes")
  (add-occ "Clothier" "6" "(Textilecraft 4 Hidework 3 Jewelcraft 3 Script aOML)" "60" "720" "yes")
  (add-occ "Courtesan" "3" "(Lovecraft 5 Intrigue 5 Singing 5 Musician 3 Dancing 3 Script aOML)" "variable" "variable" "yes")
  (add-occ "Embalmer" "7" "(Embalming 4 Woodcraft 2 Alchemy 2 Perfumery 2 Script aOML)" "48" "576" "yes")
  (add-occ "Glasswork" "7" "(Glassworking 4 Ceramics 2 Alchemy 2 Script aOML)" "66" "792" "yes")
  ;; Harper opens with 2x musician
  ;; Skald opens with 2x musician
  (add-occ "Hideworker" "5" "(Hidework 4 Textilecraft 2 Alchemy 2)" "60" "720" "yes")
  ;; Innkeeper opens with 2nd lang
  (add-occ "Jeweler" "7" "(Jewelcraft 4 Metalcraft 3 Mining 2 Script aOML)" "66" "792" "yes")
  ;; Lexigrapher opens with parchment/vellum inkcraft & 2x scripts
  ;; Litigant opens with 2nd lang
  (add-occ "Locksmith" "6" "(Lockcraft 4 Metalcraft 3 Woodcraft 2 Script aOML)" "60" "720" "yes")
  ;; Shek-Pvar dependent on convocation
  (add-occ "Lyahvi" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Legerdemain 3 Glassworking 3 Jewelcraft 3)" "NIL" "NIL" "NIL")
  (add-occ "Peleahn" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Alchemy 3 Cookery 4 Metalcraft 1 Weaponcraft 1)" "NIL" "NIL" "NIL")
  (add-occ "Jmorvi" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Metalcraft 3 Lockcraft 3 Mineralogy 2 Weaponcraft 2)" "NIL" "NIL" "NIL")
  (add-occ "Fyvria" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Herblore 3 Agriculture 3 Animalcraft 2 Embalming 2 Foraging 3 Physician 2 Survival 3 Tracking 2)" "NIL" "NIL" "NIL")
  (add-occ "Odvishe" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Swimming 4 Fishing 3 Piloting 2 Seamanship 2 Brewing 2)" "NIL" "NIL" "NIL")
  (add-occ "Savorya" "3" "(Language aOML Script aOML Script2 aOML Folklore 4 Mathematics 2 Mental_Conflict 5 Drawing 4 Runecraft 3 Tarotry 3)" "NIL" "NIL" "NIL")
  (add-occ "Mason" "7" "(Masonry 4 Woodcraft 3 Engineering 3 Mathematics 2 Script aOML)" "96" "1152" "yes")
  ;; Mercatyler opens with 1x weapon, 2nd lang
  (add-occ "Metalsmith" "6" "(Metalcraft 4 Mining 2 Weaponcraft 2)" "72" "864" "yes")
  (add-occ "Miller" "6" "(Milling 4 Engineering 3 Agriculture 3 Script aOML)" "84" "1008" "yes")
  (add-occ "Milwright" "6" "(Milling 4 Engineering 3 Agriculture 3 Script aOML)" "84" "1008" "yes")
  (add-occ "Miner" "6" "(Mining 4 Engineering 3 Woodcraft 2 Metalcraft 2 Jewelcraft 1)" "84" "1008" "yes")
  (add-occ "Ostler" "5" "(Horsecraft 4 Riding 4 Hidework 3)" "78" "936" "yes")
  (add-occ "Perfumer" "6" "(Perfumery 4 Alchemy 3 Herblore 3 Embalming 2 Script aOML)" "66" "792" "yes")
  (add-occ "Physician" "7" "(Physician 4 Herblore 3 Alchemy 3 Script aOML)" "72" "864" "yes")
  ;; Pilot opens with 1x weapon
  (add-occ "Potter" "6" "(Ceramics 4 Glassworking 2)" "60" "720" "yes")
  (add-occ "Salter" "4" "(Mining 3 Cookery 4 Survival 3 Fishing 3 Herblore 1)" "48" "576" "yes")
  (add-occ "Seaman" "5" "(Seamanship 4 Climbing 5 Club 5 Dagger 4 Fishing 3 Weatherlore 3 Piloting 2 Shipwright 2)" "48" "576" "yes")
  (add-occ "Shipwright" "7" "(Shipwright 4 Woodcraft 3 Timbercraft 3 Metalcraft 2 Mathematics 2 Seamanship 2 Script aOML)" "90" "1080" "yes")
  (add-occ "Tentmaker" "5" "(Textilecraft 3 Hidework 3 Woodcraft 2)" "72" "864" "yes")
  (add-occ "Thespian" "7" "(Acting 4 Oratory 4 Singing 4 Musician 3 Drawing 3)" "variable" "variable" "yes")
  (add-occ "Thief" "3" "(Legerdemain 4 Awareness 5 Stealth 4 Intrigue 4 Lockcraft 3 Club 5 Dagger 4 Acrobatics 2)" "variable" "variable" "yes")
  (add-occ "Timberwright" "6" "(Timbercraft 4 Survival 3 Weatherlore 4 Woodcraft 3)" "78" "936" "yes")
  ;; Weaponcraft opens with 2x weapons
  (add-occ "Woodcrafter" "7" "(Woodcraft 5 Metalcraft 2 Hidework 2)" "66" "792" "yes")
  ;; Clean up Military Wages & Years
  (add-occ "Feudal Militia - UF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 4 Roundshield 4)" "0" "0" "no")
  (add-occ "Feudal Yeoman - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 5 Shortsword 4 Dagger 4 Roundshield 4)" "0" "0" "no")
  (add-occ "Feudal Guardsman - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Shortsword 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "Feudal Guardsman - MF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Falchion 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "Feudal Yeoman - SB" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Shortbow 5 Falchion 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "Feudal Yeoman - LB" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Longbow 5 Falchion 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "Feudal Knight - MH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lance 6 Bastard_Sword 5 Handaxe 5 Dagger 5 Kite_Shield 6 Dancing 3)" "0" "0" "no")
  (add-occ "Feudal Knight - HH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lance 6 Broadsword 5 Handaxe 5 Dagger 5 Kite_Shield 6 Dancing 3)" "0" "0" "no")
  (add-occ "FO Infantry - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Shortsword 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "FO Infantry - MF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Falchion 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "FO Archer - SB" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Shortbow 5 Shortsword 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "FO Knight - MH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lance 6 Broadsword 5 Mace 5 Dagger 5 Kite_Shield 6 Dancing 3)" "0" "0" "no")
  (add-occ "FO Knight - HH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lange 6 Bastard_Sword 5 Mace 5 Dagger 5 Kite_Shield 6 Dancing 5)" "0" "0" "no")
  (add-occ "Imperial Militia - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 4 Roundshield 4)" "0" "0" "no")
  (add-occ "Imperial Legionnaire - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 5 Shortsword 4 Dagger 4 Tower_Shield 5)" "0" "0" "no")
  (add-occ "Imperial Legionnaire - MF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Shortsword 5 Dagger 4 Tower_Shield 5)" "0" "0" "no")
  (add-occ "Imperial Patrician - MH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lange 6 Broadsword 5 Handaxe 5 Dagger 5 Kite_Shield 5 Dancing 3)" "0" "0" "no")
  (add-occ "Viking Clansman - UF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 5 Roundshield 5 Keltan 4 Seamanship 3)" "0" "0" "no")
  (add-occ "Viking Clansman - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Roundshield 6 Shortbow 5 Keltan 5 Seamanship 3)" "0" "0" "no")
  (add-occ "Viking Huscarl - MF" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Batlleaxe 6 Broadsword 5 Keltan 5 Roundshield 5 Seamanship 4 Piloting 2)" "0" "0" "np")
  (add-occ "Viking Huscarl - LH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Lance 6 Broadsword 5 Roundshield 5 Seamanship 3 Piloting 2)" "0" "0" "no")
  (add-occ "Khuzdul Clansman - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Handaxe 5 Roundshield 5)" "0" "0" "no")
  (add-occ "Khuzdul Low Guard - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 6 Battleaxe 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "Khuzdul High Guard - HF" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Poleaxe 6 Mace 5 Dagger 5 Roundshield 5)" "0" "0" "no")
  (add-occ "Sindarin Ranger - UF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Hartbow 6 Longknife 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "Sindarin Ranger - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Hartbow 6 Longknife 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "Sindarin Guardian - MF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Spear 7 Longknife 5 Dagger 5 Buckler 5)" "0" "0" "no")
  (add-occ "Sindarin Horsebow - LF" "0" "(Initiative 5 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 6 Hartbow 6 Lance 5 Longknife 5 Knight_Shield 6 Dancing 3 Musician 2)" "0" "0" "no")
  (add-occ "Sindarin Knight - MH" "0" "(Initiative 6 Foraging 4 Survival 4 Heraldry 2 Physician 2 Weaponcraft 2 Riding 7 Lance 7 Longknife 6 Knight_Shield 6 Dancing 3 Musician 2)" "0" "0" "no"))
  ;; Noble still needed, Military years + wages too

(defun set-the-stage ()
  (if (equal (probe-file databaseloc) NIL)
      (progn
	(format t "Database does not exist at ~a~%" databaseloc)
	(open databaseloc :direction :probe :if-does-not-exist :create)
	(create-schema)
	(populate-skill-defs)
	(populate-occupation-defs))
      (format t "Database already exists~%")))

;;===== Calculation Functions =====
